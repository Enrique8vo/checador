﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConexionBD;
using Entidades;
using System.Data;

namespace AccesoDatosChecador
{
    public class Accesotipopersona
    {
        Conexion _conexion;
        public Accesotipopersona()
        {
            _conexion = new Conexion("localhost", "root", "", "checador", 3306);
        }
        public void EliminarPersona(int idtipopersona)
        {
            string cadena = string.Format("delete from tipopersona where idtipopersonal = {0} ", idtipopersona);
            _conexion.EjecutarConsulta(cadena);
        }
        public void GuardarPersona(EtipoPersona persona)
        {
            if (persona.Idpersonal == 0)
            {
                string cadena = string.Format("insert into tipopersona values(null, '{0}')", persona.Nombre);
                _conexion.EjecutarConsulta(cadena);
            }
            else
            {
                string cadena = string.Format("update tipopersona set nombre = '{0}' where idtipopersonal= {1}", persona.Nombre, persona.Idpersonal);
                _conexion.EjecutarConsulta(cadena);
            }
        }
        public List<EtipoPersona> ObtenerListaPersona(string filtro)
        {
            var list = new List<EtipoPersona>();
            string consulta = string.Format("Select * from tipopersona where idtipopersonal like '%{0}%'", filtro);
            var ds = _conexion.ObtenerDatos(consulta, "tipopersonal");
            var dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var personal = new EtipoPersona
                {
                    Idpersonal = Convert.ToInt32(row["idtipopersonal"]),
                    Nombre = row["Tipopersonal"].ToString(),
                };
                list.Add(personal);
            }
            return list;
        }
    }
}