﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entidades;
using LogicadeNegoio;

namespace Checador
{
    public partial class Form1 : Form
    {
        private logicachecadas _lchecadas;
        private echecadas _echecadas;
        private bool _isEnablebinding = false;
        private Timer ti;
        public Form1()
        {
            ti = new Timer();
            ti.Tick += new EventHandler(eventoTimer);
            InitializeComponent();
            ti.Enabled = true;
            _lchecadas = new logicachecadas();
            _echecadas = new echecadas();
            _isEnablebinding = true;
            Bindingchecadas();
        }
        public Form1(echecadas personal)
        {
            InitializeComponent();
            _lchecadas = new logicachecadas();
            _echecadas = new echecadas();
            _echecadas = personal;
            BindingchecadasReload();
            _isEnablebinding = true;
            //BindingPersonal();
        }
        private void eventoTimer(object ob, EventArgs evt)
        {
            DateTime hoy = DateTime.Now;
            lblreloj.Text = hoy.ToString("hh:mm");
            lblfecha.Text = DateTime.Now.ToLongDateString();
        }
        private void BindingchecadasReload()
        {
            lblreloj.Text = _echecadas.Hora;
            lblfecha.Text = _echecadas.Fecha;
           txtrfc.Text = _echecadas.Fkrfc;


        }
        private void Bindingchecadas()
        {
            if (_isEnablebinding)
            {
                if (_echecadas.Hora == _echecadas.Hora)
                {
                    _echecadas.Hora = _echecadas.Hora;
                }
                
             
                _echecadas.Hora = lblreloj.Text;
                _echecadas.Fecha = lblfecha.Text;
                _echecadas.Fkrfc = txtrfc.Text;

            }
        }
        private void Guardar()
        {
            _lchecadas.Guardar(_echecadas);
        }
        private void Buscarchecadas()
        {
            _lchecadas.obtener(_echecadas);
        }
        private void button1_Click(object sender, EventArgs e)
        {
            Menu menu = new Menu();
            menu.ShowDialog();
        }

        private void groupBox2_Enter(object sender, EventArgs e)
        {
            
        }

        private void textBox1_Enter(object sender, EventArgs e)
        {
          
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                Bindingchecadas();
                Guardar();
               // MessageBox.Show(Buscarchecadas());
                
            }
            else
            {
                MessageBox.Show("usuario no valido");
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void lblreloj_Click(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
