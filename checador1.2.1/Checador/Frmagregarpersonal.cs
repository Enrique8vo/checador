﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LogicadeNegoio;
using Entidades;

namespace Checador
{
    public partial class Frmagregarpersonal : Form
    {
        private Logicapersonal _lPersonal;
        private Epersonal _ePersonal;
        private bool _isEnablebinding = false;
        public Frmagregarpersonal()
        {
            InitializeComponent();
            _lPersonal = new Logicapersonal();
            _ePersonal = new Epersonal();
            _isEnablebinding = true;
            BindingPersonal();
        }
        public Frmagregarpersonal(Epersonal personal)
        {
            InitializeComponent();
            _lPersonal = new Logicapersonal();
            _ePersonal = new Epersonal();
            _ePersonal = personal;
            BindingPersonalReload();
            _isEnablebinding = true;
            BindingPersonal();
        }
        private void BindingPersonalReload()
        {
            txtRfc.Text = _ePersonal.Rfc;
            txtNombre.Text = _ePersonal.Nombre;
            txtApellidoP.Text = _ePersonal.ApellidoP;
            txtApellidoM.Text = _ePersonal.ApellidoM;
            dtpFechaNa.Text = Convert.ToString(_ePersonal.Fechadenacimiento);
            txtPersonal.Text = Convert.ToString(_ePersonal.Fktipopersonal);
        }
        private void BindingPersonal()
        {
            if (_isEnablebinding)
            {
                if (_ePersonal.Rfc == _ePersonal.Rfc)
                {
                    _ePersonal.Rfc = _ePersonal.Rfc;
                }
                try
                {
                    _ePersonal.Rfc = txtRfc.Text;
                    _ePersonal.Nombre = txtNombre.Text;
                    _ePersonal.ApellidoP = txtApellidoP.Text;
                    _ePersonal.ApellidoM = txtApellidoM.Text;
                    _ePersonal.Fechadenacimiento = Convert.ToDateTime(dtpFechaNa.Text);
                    _ePersonal.Fktipopersonal = Convert.ToInt32(txtPersonal.Text);
                }
                catch (Exception ex)
                {

                   // MessageBox.Show("Error");
                }


            }
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            _lPersonal.GuardarPersonal(_ePersonal);
        }

        private void Frmagregarpersonal_Load(object sender, EventArgs e)
        {

        }
    }
}
