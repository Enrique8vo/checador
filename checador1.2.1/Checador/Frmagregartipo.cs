﻿using Entidades;
using LogicadeNegoio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Checador
{
    public partial class Frmagregartipo : Form
    {
        private Logicatipopersonal _lTipopersona;
        private EtipoPersona _eTipopersona;

        private bool _isEnablebinding = false;
        public Frmagregartipo()
        {
            InitializeComponent();
            _lTipopersona = new Logicatipopersonal();
            _eTipopersona = new EtipoPersona();
            _isEnablebinding = true;
            BindingTipopersona();
        }
        public Frmagregartipo(EtipoPersona persona)
        {
            InitializeComponent();
            _lTipopersona = new Logicatipopersonal();
            _eTipopersona = new EtipoPersona();

            BindingTipopersonaReload();
            _eTipopersona = persona;
            _isEnablebinding = true;


        }
        private void BindingTipopersonaReload()
        {
            txtTipopersona.Text = _eTipopersona.Nombre;


        }
        private void BindingTipopersona()
        {
            if (_isEnablebinding)
            {
                if (_eTipopersona.Nombre == _eTipopersona.Nombre)
                {
                    _eTipopersona.Nombre = _eTipopersona.Nombre;
                }

                _eTipopersona.Nombre = txtTipopersona.Text;

            }
        }
        private void Guardar()
        {
            _lTipopersona.GuardarTipopersona(_eTipopersona);
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            BindingTipopersona();
            Guardar();
        }

        private void Frmagregartipo_Load(object sender, EventArgs e)
        {

        }

        private void txtTipopersona_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
