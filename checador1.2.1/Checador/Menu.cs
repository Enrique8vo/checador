﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace Checador
{
    public partial class Menu : Form
    {
        public Menu()
        {
            InitializeComponent();
        }
        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();
        [DllImport("user32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hwnd, int wmsg, int wparam, int lparam);

        private void Restaurar_Click(object sender, EventArgs e)
        {
          
            WindowState = FormWindowState.Maximized;
            Restaurar.Visible = false;
            Maximizar.Visible = true;
           
        }

        private void Maximizar_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Normal;
          Maximizar.Visible = false;
            Restaurar.Visible = true;
        }

        private void Minimizar_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }

        private void Salir_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void MenuTop_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }
        private void AbrirFormInPanel(object Formhijo)
        {
            if (this.Wrapper.Controls.Count > 0)
                this.Wrapper.Controls.RemoveAt(0);
            Form fh = Formhijo as Form;
            fh.TopLevel = false;
            fh.Dock = DockStyle.Fill;
            this.Wrapper.Controls.Add(fh);
            this.Wrapper.Tag = fh;
            fh.Show();
        }

        private void btnagregar_Click(object sender, EventArgs e)
        {
            AbrirFormInPanel(new Frmagregartipo());
        }

        private void btnagregarpersona_Click(object sender, EventArgs e)
        {
            AbrirFormInPanel(new Frmagregarpersonal());
        }

        private void btnbuscar_Click(object sender, EventArgs e)
        {
            AbrirFormInPanel(new frmbuscarpersonal());
        }

        private void btnbuscartipo_Click(object sender, EventArgs e)
        {
            AbrirFormInPanel(new frmbuscartipo());
        }
    }
}
