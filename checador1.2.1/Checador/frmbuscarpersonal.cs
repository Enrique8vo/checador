﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entidades;
using LogicadeNegoio;

namespace Checador
{
    public partial class frmbuscarpersonal : Form
    {
        private Logicapersonal _lPersonal;
        private Epersonal _ePersonal;
        public frmbuscarpersonal()
        {
            InitializeComponent();
            _lPersonal = new Logicapersonal();
            _ePersonal = new Epersonal();
        }
        private void Eliminar()
        {
            string numero = dtgPersonal.CurrentRow.Cells["rfc"].Value.ToString();
            _lPersonal.EliminarPersonal(numero);
        }
        private void BindingPersonal()
        {
            _ePersonal.Rfc = dtgPersonal.CurrentRow.Cells["rfc"].Value.ToString();
            _ePersonal.Nombre = dtgPersonal.CurrentRow.Cells["nombre"].Value.ToString();
            _ePersonal.ApellidoP = dtgPersonal.CurrentRow.Cells["apellidoP"].Value.ToString();
            _ePersonal.ApellidoM = dtgPersonal.CurrentRow.Cells["apellidoM"].Value.ToString();
            _ePersonal.Fechadenacimiento = Convert.ToDateTime(dtgPersonal.CurrentRow.Cells["fechadenacimiento"].Value.ToString());
            _ePersonal.Fktipopersonal = Convert.ToInt32(dtgPersonal.CurrentRow.Cells["fktipopersonal"].Value.ToString());
        }
        private void BuscarPersonal(string filtro)
        {
            dtgPersonal.DataSource = _lPersonal.ObtenerListaPersonal(filtro);
        }

        private void frmbuscarpersonal_Load(object sender, EventArgs e)
        {
            BuscarPersonal("");
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            Eliminar();
            BuscarPersonal("");
        }

        private void dtgPersonal_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            BindingPersonal();
          
            BuscarPersonal("");
        }
    }
}
