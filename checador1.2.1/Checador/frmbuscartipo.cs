﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entidades;
using LogicadeNegoio;

namespace Checador
{
    public partial class frmbuscartipo : Form
    {
        private Logicatipopersonal _lTipopersona;
        private EtipoPersona _eTipopersona;
        public frmbuscartipo()
        {
            InitializeComponent();
            _lTipopersona = new Logicatipopersonal();
            _eTipopersona = new EtipoPersona();
        }
        private void Eliminar()
        {
            int numero = Convert.ToInt32(dtgPersonal.CurrentRow.Cells["idpersonal"].Value.ToString());
            _lTipopersona.EliminarTipopersona(numero);
        }
        private void Bindingpersona()
        {
            _eTipopersona.Idpersonal = Convert.ToInt32(dtgPersonal.CurrentRow.Cells["idpersonal"].Value.ToString());
            _eTipopersona.Nombre = dtgPersonal.CurrentRow.Cells["nombre"].Value.ToString();
        }
        private void BuscarPersona(string filtro)
        {
            dtgPersonal.DataSource = _lTipopersona.ObtenerListaTipopersona(filtro);
        }

        private void frmbuscartipo_Load(object sender, EventArgs e)
        {
            BuscarPersona("");
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            Eliminar();
            BuscarPersona("");
        }

        private void dtgPersonal_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            Bindingpersona();
            
            BuscarPersona("");
        }
    }
}
