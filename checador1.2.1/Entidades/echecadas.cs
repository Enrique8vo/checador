﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class echecadas
    {
        private int idchecada;
        private string hora;
        private string fecha;
        private string fkrfc;

        public int Idchecada { get => idchecada; set => idchecada = value; }
        public string Hora { get => hora; set => hora = value; }
        public string Fecha { get => fecha; set => fecha = value; }
        
        public string Fkrfc { get => fkrfc; set => fkrfc = value; }
    }
}
