﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AccesoDatosChecador;
using Entidades;

namespace LogicadeNegoio
{
    public class Logicapersonal
    {
        private AccesoDarosPersonal _aPersonal;
        public Logicapersonal()
        {
            _aPersonal = new AccesoDarosPersonal();
        }
        public void EliminarPersonal(string rfc)
        {
            _aPersonal.EliminarPersonal(rfc);
        }
        public void GuardarPersonal(Epersonal personal)
        {
            _aPersonal.GuardarPersonal(personal);
        }
        public List<Epersonal> ObtenerListaPersonal(string filtro)
        {
            var list = new List<Epersonal>();
            list = _aPersonal.ObtenerListaPersonal(filtro);
            return list;
        }
    }
}