﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using AccesoDatosChecador;



namespace LogicadeNegoio
{
    public class Logicatipopersonal
    {
        private Accesotipopersona  _aTipopersona;
        public Logicatipopersonal()
        {
            _aTipopersona = new Accesotipopersona();
        }
        public void EliminarTipopersona(int idtipopersona)
        {
            _aTipopersona.EliminarPersona(idtipopersona);
        }
        public void GuardarTipopersona(EtipoPersona persona)
        {
            _aTipopersona.GuardarPersona(persona);
        }
        public List<EtipoPersona> ObtenerListaTipopersona(string filtro)
        {
            var list = new List<EtipoPersona>();
            list = _aTipopersona.ObtenerListaPersona(filtro);
            return list;
        }
    }
}

