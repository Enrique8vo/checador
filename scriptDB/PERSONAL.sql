create database checador;
use checador;

create table tipopersona(
IDTIPOPERSONAL  INT AUTO_INCREMENT PRIMARY KEY,
TIPOPERSONAL varchar(100)
);

create table personal(
RFC varchar(100) PRIMARY KEY ,

NOMBRE varchar(50), 
APELLIDOP varchar(50), 
APELLIDOM varchar(50),
FECHADENACIMIENTO date,
FKTIPOPERSONAL int,
foreign key (FKTIPOPERSONAL) REFERENCES TIPOPERSONA (IDTIPOPERSONAL)
);