
use checador;

CREATE TABLE tiempos
(
    ID int(9) primary key auto_increment,
    hora_entrada Datetime NULL,
    hora_salida  Datetime NULL
);